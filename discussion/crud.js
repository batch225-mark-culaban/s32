let http = require("http");

    // Mock database  
let directory = [
    {
        "name" : "Rendon", 
        "email" : "rendon@gmail.com"

    },
    {
        "name" : "Jobert", 
        "email" : "jobert@email.com"  
    },
    {
        "name" : "CongTV", 
        "email" : "CongTV@email.com"  
    },
    {
        "name" : "PINGRIS", 
        "email" : "PINGRIS@email.com"  
    },
];


http.createServer(function (request, response){

if(request.url == "/users" && request.method == "GET"){

    response.writeHead(200, {'Content-Type' : 'application/json'})
;
    
    //JSON.stringify() - transform a javascript into JSON string
    response.write(JSON.stringify(directory));
    response.end();
}
    // pwede pd "if" lng
    // Route for creating a new item upon recieving a POST request 
    // a request object contains several parts:
    // HEADERS - conrtains information about the request context/content  like what is the data type.
    // BODY - contain the actual information being sent with the request.
    //Note: The request body can NOT be empty
    else if(request.url == "/users" && request.method == "POST")
{

    // Declare and initialize a  "requestBody" variable to an empty string
    // This will act as a placeholder for the resource/data to be created later on.
    let requestBody = '';



    // A stream is a sequence of data
    // Data is recieve from the client and is processed in the 'data' stream
    // The information provided from the request object enters a sequence called "data" the code below will be triggered
    // data step - this reads the 'data' stream and processes is as the request body

    // the "ON" method binds an event to a object
    // It's a way to express your intent of there is something happening (data sent or error in our case), then execute the function added as a parameter. This style of programming is called "Event-driven Programming"
    //event-driven-programming is a programming paradigm in which the flow of the program is determined by event such as user actions(mouse clicks, key presses) or message passing from other programs or threads.

    /*
    example

    paulo - CEO
    charicez - crew

    */



    request.on('data', function(data){


    
        requestBody += data

    });

    request.on('end', function(){
        // checks if at this point the requestBody is of data type STRING
        // we need this to be of data type JSON to access its property.

            console.log(typeof requestBody);

        requestBody = JSON.parse(requestBody);
        //Converts the JSON string requestBody to Javascript
        // Json.parse() - Takes a JSON string and transform it into javascript objects.

        // Create a new object representing the new mock database record

        let newUser = {
            "name" : requestBody.name,
            "email" : requestBody.email
        }
            //example format input sa postman
            /*
            {
                     "name" : "Johna",
                      "email" : "johna@gmail.com"
             }
             */
        // Add the user into the mock database 
        directory.push(newUser);
        console.log(directory);

        response.writeHead(200, {'Content-Type' : 'application/json'});
        response.write(JSON.stringify(newUser));

    });
}

}).listen(4000);

console.log('Server running at localhost:4000');
