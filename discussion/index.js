// Node.js Routing with HTTP methods

let http = require("http");


const server = http.createServer(function (request, response) {
    // The HTTP methods of the incoming request can be accessed via the "method" property of the "request" parameter.
    // The method "GET" means that we will be retrieving or reading information or data
    if(request.url == "/items" && request.method == "GET") {
        
        response.writeHead(200, {'Content-Type' : 'text/plain'});
        response.end('Data retrieve from database');

    }

    // The moethod "POST" means that we will be addingor creating information but for now, we will just be sending a text response.
    if(request.url == "/items" && request.method == "POST") {
        
        response.writeHead(200, {'Content-Type' : 'text/plain'});
        response.end('To be sent through the Database');
 
    }



}).listen(4000);
console.log('Server is running at localhost:4000');