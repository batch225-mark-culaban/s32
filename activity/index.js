

let http = require("http");


const server = http.createServer(function (request, response) {
    // The HTTP methods of the incoming request can be accessed via the "method" property of the "request" parameter.
    // The method "GET" means that we will be retrieving or reading information or data
    if(request.url == "/courses" && request.method == "GET") {
        
        response.writeHead(200, {'Content-Type' : 'text/plain'});
        response.end('Here our courses available');

    }

    // The moethod "POST" means that we will be addingor creating information but for now, we will just be sending a text response.
    if(request.url == "/addCourse" && request.method == "POST") {
        
        response.writeHead(200, {'Content-Type' : 'text/plain'});
        response.end('Add course to our resources');
 
    }



}).listen(4000);
console.log('Server is running at localhost:4000');

/*
localhost:4000/courses
localhost:4000/addCourse
*/